package edu.lsus.chaticus

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (!app.session.hasValidCredentials()) {
            app.login(this)
        }
        else {
            app.api.getMessages(1).enqueue(object : Callback<List<MessageDto>> {
                override fun onResponse(call: Call<List<MessageDto>>?, response: Response<List<MessageDto>>?) {

                }

                override fun onFailure(call: Call<List<MessageDto>>?, t: Throwable?) {

                }
            })
        }
    }
}